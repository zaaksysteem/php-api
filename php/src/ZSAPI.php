<?php

class ZSAPI
{
    public $version = 0.1;

    /** @var string Should contain the hostname of the API */
    public $apiHostname;

    /** @var string Should contain the username of the API */
    public $apiUsername;

    /** @var string Should contain the password of the API */
    public $apiPassword;

    /** @var integer should contain the identifier of the api */
    public $apiIdentifier;


    private $debugStack = array();

    /**
     * This method returns the version of JAPI
     *
     * @return string
     */

    public function getVersion() {
        return $this->version;
    }

    /**
     * Dispatches the given params to the given url
     *
     * Returns a php object representing the returned json
     */
    public function dispatch($url, $params) {
        $curl       = $this->_setupCurl($url);
        $request    = json_encode($params);

        $this->debug('JSON Request: ' . $request);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);

        $response   = curl_exec($curl);

        if($errno = curl_errno($curl)) {
            $error_message = curl_error($curl);
            throw new Exception('There was an error calling curl: ' . $error_message);
        }

        $this->debug('Response: ' . $response);
        $status     = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ( !$status ) {
            throw new Exception("CURL Error, no return status");
        }

        if (!preg_match('/application\/json/', curl_getinfo($curl, CURLINFO_CONTENT_TYPE))) {
            throw new Exception("Invalid content-type, no JSON returned: " . curl_getinfo($curl, CURLINFO_CONTENT_TYPE));
        }

        $jresponse  = json_decode($response, true);

        curl_close($curl);

        return $this->handleResponse($jresponse);
    }

    /**
     * Setups the CURL instance
     */
    private function _setupCurl($url) {
        if (!$this->apiHostname) {
            throw new Exception("No API hostname given");
        }

        $this->debug('Load curl for hostname: ' . $this->apiHostname . $url);
        $curl       = curl_init($this->apiHostname . $url);

        $headers = array(
            "Content-type: application/json"
        );

        if ($this->apiIdentifier) {
            array_push($headers, 'API-Interface-Id: ' . $this->apiIdentifier);
            $this->debug('Loaded header: API-Interface-Id: ' . $this->apiIdentifier);
        }

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 40);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_DIGEST);
        curl_setopt($curl, CURLOPT_USERPWD, $this->apiUsername . ":" . $this->apiPassword);

        $this->debug('Authenticating with user: ' . $this->apiUsername);

        return $curl;
    }

    /**
     * Puts a log entry on the stack
     */
    protected function debug($message) {
        array_push($this->debugStack, $message);
    }

    /**
     * Returns an array of log entries
     */
    public function log() {
        return $this->debugStack;
    }

    /**
     * Handles a response
     */
    public function handleResponse($response) {
        if (!$response) {
            throw new Exception('Got invalid response');
        }

        if ($response['result']['type'] == 'exception') {
            return new ZSAPI_Exception($response);
        }

        return new ZSAPI_Response($response);
    }
}

class ZSAPI_Exception
{
    public $message;

    public $type;

    public $statuscode;

    public $is_error = 1;

    public function __construct($response) {
        $this->message      = $response['result']['instance']['message'];
        $this->type         = $response['result']['instance']['type'];
        $this->statuscode   = $response['status_code'];
    }
}

class ZSAPI_Response
{
    public $type;

    public $instance;

    public $reference;

    public $is_error;

    public function __construct($response) {
        $this->type         = $response['result']['type'];
        $this->instance     = $response['result']['instance'];
        $this->reference    = (isset($response['result']['reference']) ? $response['result']['reference'] : null);

        if ($this->type == 'validationexception') {
            $this->is_error = 1;
        }
    }
}
