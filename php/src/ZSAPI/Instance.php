<?php

include_once(dirname(dirname(__FILE__)) . '/ZSAPI.php');

class ZSAPI_Instance extends ZSAPI
{
    public $controlpanel;
    public $owner;
    public $label;
    public $fqdn;
    public $otap = 'production';
    public $password;
    public $freeform_reference;

    public function __construct($params = array()) {
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if (!property_exists(__CLASS__, $key)) { continue; }

                $this->$key = $value;
            }
        }
    }

    public function create() {
        $this->_checkFields();

        $params = array(
            'controlpanel'          => $this->owner,
            'label'                 => $this->label,
            'fqdn'                  => $this->_checkHost($this->fqdn),
            'otap'                  => $this->otap,
            'password'              => $this->password,
            'freeform_reference'    => $this->freeform_reference,
        );

        return $this->dispatch('/api/v1/controlpanel/' . $this->controlpanel . '/instance/create', $params);
    }

    private function _checkFields() {
        if (!$this->controlpanel || !$this->fqdn || !$this->password) {
            throw new Exception('Required fields controlpanel and/or fqdn are missing');
        }
    }

    private function _checkHost($host) {
        if (!preg_match('/^[a-zA-Z][a-z-A-Z0-9-]+$/', $host)) {
            throw new Exception('Make sure host only contains word characters, digits or dashes');
        }

        return $host;
    }
}

?>
