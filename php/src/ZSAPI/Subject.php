<?php

include_once(dirname(dirname(__FILE__)) . '/ZSAPI.php');

class ZSAPI_Subject extends ZSAPI
{

    /** @var string subject_type */
    public $subject_type = 'company';

    /** @var string password */
    public $password;

    /** @var array subject_params */
    public $subject_params = array();

    public function __construct($params = array(), $subject_params = array()) {
        if (is_array($params)) {
            foreach ($params as $key => $value) {
                if (!property_exists(__CLASS__, $key)) { continue; }

                $this->$key = $value;
            }
        }

        foreach ($subject_params as $key => $value) {
            $this->subject_params[$key] = $value;
        }
    }

    public function is_subject_available() {
        if ($this->subject_type != 'company') {
            throw new Exception('is_subject_available only allowed for companies');
        }

        if (!$this->subject_params['subject']['coc_number'] || !$this->subject_params['subject']['coc_location_number']) {
            throw new Exception('is_subject_available needs at least a coc_location_number and coc_number');
        }

        ### Remove handelsnaam temporarily, to make sure this thing will die
        $handelsnaam = $this->subject_params['subject']['company'];
        $this->subject_params['subject']['company'] = '';

        $response = null;
        try {
            $response = $this->create();
        } catch (Exception $e) {
            $this->debug('Something went wrong in checking subject availability, default to 0: ' . $e);
        };

        $this->subject_params['subject']['company'] = $handelsnaam;

        if ($response && $response->is_error && $response->type == 'validationexception' && $response->instance['coc_number']['validity'] == 'valid') {
            return 1;
        }

        return 0;
    }

    public function create() {
        $this->_checkFields();

        $params = array(
            'subject_type'      => $this->subject_type,
        );

        if ($this->password) {
            $params['authentication'] = array(
                'password' => $this->password
            );

            if ($this->subject_type == 'company') {
                $params['authentication']['username'] = $this->subject_params['coc_number'];
            }
        }

        $params = array_merge($params, $this->subject_params);

        return $this->dispatch('/api/v1/subject/create',$params);
    }

    private function _checkFields() {
        if (!$this->subject_params || !$this->subject_type) {
            throw new Exception('Required fields subject_params or subject_type are missing');
        }
    }
}

?>
