<?php

include_once(dirname(dirname(__FILE__)).  '/src/ZSAPI/Controlpanel.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Instance.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Subject.php');
include_once(dirname(dirname(__FILE__)). '/src/ZSAPI/Case.php');

class ZSAPITest extends PHPUnit_Framework_TestCase
{

    private function _create_subject()
    {
        $s = new ZSAPI_Subject();

        $s->apiHostname     = 'https://10.44.0.11';
        $s->apiUsername     = 'beheerder';
        $s->apiPassword     = '12345678';
        $s->subject_type    = 'bedrijf';
        $s->subject_params  = array(
            'handelsnaam'                       => 'Gemeente Zaakstad',
            'dossiernummer'                     => '25654239',
            'vestigingsnummer'                  => '123456789012',
            'vestiging_landcode'                => '6030',
            'vestiging_straatnaam'              => 'Zaakstraat',
            'vestiging_huisnummer'              => 44,
            'vestiging_huisletter'              => 'a',
            'vestiging_huisnummertoevoeging'    => '1rechts',
            'vestiging_postcode'                => '1234AB',
            'vestiging_woonplaats'              => 'Amsterdam',
        );

        try {
            $subject = $s->create();
        } catch (Exception $e) {
            var_dump($s->log());
            print $e;
        }

        if ($subject->is_error) {
            var_dump($s->log());
        }

        return $subject;
    }

    private function _create_controlpanel($subject) {
        $c = new ZSAPI_Controlpanel();

        $c->apiHostname     = 'https://10.44.0.11';
        $c->apiUsername     = 'beheerder';
        $c->apiPassword     = '12345678';

        $c->owner           = $subject->instance['subject_identifier'];
        $c->customer_type   = 'commercial';

        if (!$c->is_host_available('test234.zaaksysteem.net')) {
            throw new Exception('Stop maar...niet available');
        }

        try {
            $cp = $c->create();
        } catch (Exception $e) {
            var_dump($c->log());
            print $e;
        }

        return $cp;
    }

    private function _create_instance($cp) {
        $i = new ZSAPI_Instance();

        $i->apiHostname     = 'https://10.44.0.11';
        $i->apiUsername     = 'beheerder';
        $i->apiPassword     = '12345678';

        $i->fqdn            = 'test22';
        $i->label           = 'Testomgeving';
        $i->controlpanel    = $cp->reference;

        try {
            $instance = $i->create();
        } catch (Exception $e) {
            var_dump($i->log());
            print $e;
        }

        return $instance;
    }

    // public function testCreate() {
    //     $subject = $this->_create_subject();
    //     var_dump($subject->log());
    //     $this->assertContains($subject->type, 'subject');
    //     $this->assertContains($subject->instance['handelsnaam'], 'Gemeente Zaakstad');

    //     // $cp      = $this->_create_controlpanel($subject);
    //     // $instance = $this->_create_instance($cp);

    //     // echo "\nCreated controlpanel for: " . $subject->instance['subject_identifier'];
    //     // echo "\n  controlpanel: " . $cp->reference;
    //     // echo "\n    instance: " . $instance->reference . ' / ' . $instance->instance['fqdn'];
    // }

    public function testNinja() {
        $input = array(
            'case'  => array(
                'voornaam'                      => 'Michiel',
                'achternaam'                    => 'Ootjers',
                'contactpersoon_email'          => 'michiel@ootjers.nl',
                'contactpersoon_telefoon'       => '0634567890',
            ),
            'subject' => array(
                'vestigingsnummer'              => '123456789012',
                'dossiernummer'                 => '37103676',
                'handelsnaam'                   => 'Mintlab B.V.',
                'vestiging_straatnaam'          => 'Donker Curtiusstraat',
                'vestiging_huisnummer'          => '7',
                'vestiging_huisletter'          => 'a',
                'vestiging_huisnummertoevoeging' => 'unit 521',
                'vestiging_postcode'            => '1051JL',
                'vestiging_woonplaats'          => 'Amsterdam',
            ),
            'cp'      => array(
                'fqdn'  => 'test6'
            )
        );

        zsapi_post_params($input);
    }

}

require_once(dirname(dirname(__FILE)) . '/src/wordpress/functions.zsapi.php');

    
